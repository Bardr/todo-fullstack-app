import { BrowserRouter as Router, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';

import Header from './components/Header';
import Tasks from './components/Tasks';
import AddTask from './components/AddTask';
import Footer from './components/Footer';
import About from './components/About';
import EditTask from './components/EditTask';

function App() {
  const [showAddTask, setShowAddTask] = useState(false);
  const [taskToEdit, setTaskToEdit] = useState(null);
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    const getTasks = async () => {
      const tasksFromServer = await fetchTasks();
      setTasks(tasksFromServer);
    };

    getTasks();
  }, []);

  const fetchTasks = async () => {
    const res = await fetch('http://localhost:8080/api/todos');
    return await res.json();
  };

  const deleteTask = async (id) => {
    await fetch(`http://localhost:8080/api/todos/${id}`, { method: 'DELETE' });

    setTasks(tasks.filter((task) => task.id !== id));
  };

  const toggleReminder = async (id, reminder) => {
    const res = await fetch(`http://localhost:8080/api/todos/${id}/reminder`, {
      method: 'PUT',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({ remind: reminder }),
    });
    const data = await res.json();

    setTasks(
      tasks.map((task) =>
        task.id === id ? { ...task, reminder: data.reminder } : task
      )
    );
  };

  const addTask = async (task) => {
    const res = await fetch('http://localhost:8080/api/todos', {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(task),
    });
    const newTask = await res.json();

    setTasks([...tasks, newTask]);
  };

  const editTask = async (editedTask) => {
    setTaskToEdit(null);

    const res = await fetch(`http://localhost:8080/api/todos/${editedTask.id}`, {
      method: 'PUT',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({...editedTask, id: undefined}),
    });
    const editedTaskBackend = await res.json();

    setTasks([
      ...tasks.map((task) => (task.id === editedTaskBackend.id ? editedTaskBackend : task)),
    ]);
  };

  return (
    <Router>
      <div className='container'>
        <Header
          onAdd={() => {
            setShowAddTask(!showAddTask);
            setTaskToEdit(null);
          }}
          showAddTask={showAddTask}
        />

        <Route
          path='/'
          exact
          render={() => (
            <>
              {showAddTask && <AddTask onAdd={addTask} />}
              {taskToEdit && <EditTask onEdit={editTask} task={taskToEdit} />}

              {!!tasks.length ? (
                <Tasks
                  tasks={tasks}
                  onDelete={deleteTask}
                  onToggle={toggleReminder}
                  onEdit={(taskId) => {
                    setTaskToEdit(tasks.find((task) => task.id === taskId));
                    setShowAddTask(false);
                  }}
                />
              ) : (
                'No more tasks! Good job 🤞'
              )}
            </>
          )}
        />

        <Route path='/about' component={About} />

        <Footer />
      </div>
    </Router>
  );
}

export default App;
