import { useMemo } from 'react';
import { FaEdit, FaTimes } from 'react-icons/fa';

const Task = ({ task, onDelete, onToggle, onEdit }) => {
  const formattedDate = useMemo(() => {
    const date = new Date(task.day);

    return date
      .toISOString()
      .replace('T', ' ')
      .slice(0, date.toISOString().length - 5);
  }, [task.day]);

  return (
    <div
      className={`task ${task.reminder ? 'reminder' : ''}`}
      onDoubleClick={() => onToggle(task.id, !task.reminder)}
    >
      <h3>
        {task.text}
        <div className='task-button-wrapper'>
          <FaTimes style={{ color: 'red' }} onClick={() => onDelete(task.id)} />
          <FaEdit
            style={{ color: 'blue', marginTop: '12px' }}
            onClick={() => onEdit(task.id)}
          />
        </div>
      </h3>

      <p>{formattedDate}</p>
    </div>
  );
};

export default Task;
