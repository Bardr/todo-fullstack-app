import TaskForm from './TaskForm';

const AddTask = ({ onAdd }) => {
  return (
    <TaskForm onValid={onAdd} />
  );
};

export default AddTask;
