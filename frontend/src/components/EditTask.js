import TaskForm from './TaskForm';

const EditTask = ({ onEdit, task }) => {
  return <TaskForm onValid={onEdit} task={task} />;
};

export default EditTask;
