import { useState, useEffect } from 'react';

const TaskForm = ({ onValid, task }) => {
  const [text, setText] = useState('');
  const [day, setDay] = useState('');
  const [reminder, setReminder] = useState(false);

  useEffect(() => {
    if (!task) {
      return;
    }

    const taskDayToTransform = new Date(
      new Date(task.day).toString().split('GMT')[0] + ' UTC'
    );
    taskDayToTransform.setMinutes(
      taskDayToTransform.getMinutes() + taskDayToTransform.getTimezoneOffset()
    );

    setText(task.text);
    setDay(taskDayToTransform.toISOString().split('.')[0]);
    setReminder(task.reminder);
  }, [task]);

  const onSubmit = (e) => {
    e.preventDefault();

    if (!text) {
      alert('Please add task!');
      return;
    }

    onValid({ text, day, reminder, id: task ? task.id : undefined });

    setText('');
    setDay('');
    setReminder(false);
  };

  return (
    <form className='add-form' onSubmit={onSubmit}>
      <div className='form-control'>
        <label>Task</label>
        <input
          type='text'
          placeholder='Add Task'
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
      </div>

      <div className='form-control'>
        <label>Day & Time</label>
        <input
          type='datetime-local'
          placeholder='Add Day & Time'
          value={day}
          onChange={(e) => setDay(e.target.value)}
        />
      </div>

      <div className='form-control form-control-check'>
        <label>Set Reminder</label>
        <input
          type='checkbox'
          checked={reminder}
          value={reminder}
          onChange={(e) => setReminder(e.currentTarget.checked)}
        />
      </div>

      <input className='btn btn-block' type='submit' value='Save Task' />
    </form>
  );
};

export default TaskForm;
