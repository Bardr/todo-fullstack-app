package com.bardr.todoapp.controllers;

import com.bardr.todoapp.dtos.EditTodoItemDto;
import com.bardr.todoapp.dtos.SetReminderDto;
import com.bardr.todoapp.models.TodoItem;
import com.bardr.todoapp.repositories.TodoItemRepository;
import com.bardr.todoapp.services.TodoItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/todos", produces = "application/json")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TodoItemController {
    @Autowired
    private TodoItemRepository repository;

    @Autowired
    private TodoItemService service;

    @GetMapping
    public ResponseEntity<List<TodoItem>> getTodoItems() {
        var todoItems = repository.findAll();

        return ResponseEntity.ok(todoItems);
    }

    @GetMapping("{itemId}")
    public ResponseEntity<TodoItem> getTodoItem(@PathVariable Long itemId) {
        var todoItem = repository.findById(itemId);

        return ResponseEntity.of(todoItem);
    }

    @PostMapping
    public ResponseEntity<TodoItem> createNewTodoItem(@RequestBody @Valid TodoItem item) {
        return ResponseEntity.ok(service.saveTodoItem(item));
    }

    @PutMapping("{itemId}/reminder")
    public ResponseEntity<TodoItem> setTodoItemReminder(@PathVariable Long itemId,
                                                        @RequestBody @Valid SetReminderDto reminderValue) {
        return ResponseEntity.of(service.setReminder(itemId, reminderValue.remind));
    }

    @PutMapping("{itemId}")
    public ResponseEntity<TodoItem> editTodoItem(@PathVariable Long itemId,
                                                 @RequestBody @Valid EditTodoItemDto item) {
        return ResponseEntity.of(service.editTodoItem(itemId, item));
    }

    @DeleteMapping("{itemId}")
    public ResponseEntity deleteTodoItem(@PathVariable Long itemId) {
        if (service.deleteTodoItem(itemId)) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.notFound().build();
    }

}
