package com.bardr.todoapp.models;

import java.util.Date;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Data
@Entity
@Table(name = "TodoItems")
public class TodoItem {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty(message = "Podaj tekst")
    private String text;

    private boolean reminder = false;

    private Date day = new Date();
}
