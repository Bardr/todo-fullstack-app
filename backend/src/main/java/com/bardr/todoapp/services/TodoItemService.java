package com.bardr.todoapp.services;

import com.bardr.todoapp.dtos.EditTodoItemDto;
import com.bardr.todoapp.models.TodoItem;
import com.bardr.todoapp.repositories.TodoItemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("TodoItemService")
public class TodoItemService {
    @Autowired
    private TodoItemRepository todoItemRepository;

    public TodoItem saveTodoItem(TodoItem item) {
        return todoItemRepository.save(item);
    }

    public Optional<TodoItem> setReminder(Long id, boolean remind) {
        var item = todoItemRepository.findById(id).orElse(null);

        if (item == null) {
            return Optional.empty();
        }

        item.setReminder(remind);
        todoItemRepository.save(item);

        return Optional.of(item);
    }

    public Optional<TodoItem> editTodoItem(Long itemId,  EditTodoItemDto editedItem) {
        var item = todoItemRepository.findById(itemId).orElse(null);

        if (item == null) {
            return Optional.empty();
        }

        item.setReminder(editedItem.reminder);
        item.setText(editedItem.text);
        item.setDay(editedItem.day);

        return Optional.of(todoItemRepository.save(item));
    }

    public Boolean deleteTodoItem(Long id) {
        var item = todoItemRepository.findById(id).orElse(null);

        if (item == null) {
            return false;
        }

        todoItemRepository.delete(item);
        return true;
    }
}
