package com.bardr.todoapp.dtos;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

public class EditTodoItemDto {
    @NotEmpty(message = "Podaj tekst")
    public String text;

    public boolean reminder;

    public Date day = new Date();
}
